![build](https://gitlab.com/atsukisama1/stacklabtest/badges/master/build.svg)
![coverage](https://gitlab.com/atsukisama1/stacklabtest/badges/master/coverage.svg?job=coverage)

Stacklab
======

A basic REST API for stacklab technical test.

**Code coverage : https://atsukisama1.gitlab.io/stacklabtest**

Install
-------

    # clone the repository
    git clone https://gitlab.com/atsukisama1/stacklabtest
    cd stacklabtest

Create a virtualenv and activate it :

    python3 -m venv venv
    . venv/bin/activate
 
Install Stacklab :

    pip install -e .

Run
---

    export FLASK_APP=stacklab
    export FLASK_ENV=development
    flask run


Open **http://127.0.0.1:5000** in a browser.

Unitary test
----

    pip install '.[test]'
    pytest

Run with coverage report::

    coverage run -m pytest
    coverage report
    coverage html  # open htmlcov/index.html in a browser