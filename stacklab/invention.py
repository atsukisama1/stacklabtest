from flask import (Blueprint, request, jsonify)
from stacklab.db import Invention, database, init_database
from stacklab.exceptions import (StacklabException, InventionNotFound, JSONParameterNotFound,
                                 DateNotInteger, IsNotStringSize, JSONnotFound)

bp = Blueprint('inventions', __name__)


def invention_get_all():
    inventions = Invention.query.all()
    result = []
    for invention in inventions:
        result.append(invention.to_dict())
    result = sorted(result, key = lambda i: (i['date'], i['name']))
    response = {
        'data': result,
        'status_code': 200
    }
    return response


def invention_get(id):
    try:
        result = Invention.query.filter(Invention.id == id).first()
        if result is None:
            raise InventionNotFound
        output = {
            'data': result.to_dict(),
            'status_code': 200
        }
    except StacklabException as exception:
        output = {
            'data': {'success': True, 'message': exception.message},
            'status_code': exception.status_code
        }
    return output


def invention_init():
    Invention.query.delete()
    database.session.commit()
    init_database()
    output = {
        'data': {'success': True},
        'status_code': 200
    }
    return output


def invention_delete(id):
    try:
        result = Invention.query.filter(Invention.id == id).first()
        if result is None:
            raise InventionNotFound
        database.session.delete(result)
        database.session.commit()
        output = {
            'data': {'success': True},
            'status_code': 200
        }
    except StacklabException as exception:
        output = {
            'data': {'success': False, 'message': exception.message},
            'status_code': exception.status_code
        }
    return output


def invention_add(data):
    try:
        if data is None:
            raise JSONnotFound
        date = None if 'date' not in data else data['date']
        name = None if 'name' not in data else data['name']
        inventor = None if 'inventor' not in data else data['inventor']
        origin = None if 'origin' not in data else data['origin']
        site = None if 'site' not in data else data['site']
        if date is None or name is None:
            raise JSONParameterNotFound('\'date\' and \'name\'')
        #Comparing type() because isinstance does not work as intended (especially for str)
        if type(date) is not type(int()):
            raise DateNotInteger
        if type(name) is not type(str()) or len(name) > 256:
            raise IsNotStringSize('name', 256)
        if (type(inventor) is not type(str()) or len(inventor) > 64) and inventor:
            raise IsNotStringSize('inventor', 64)
        if (type(origin) is not type(str()) or len(origin) > 512) and origin:
            raise IsNotStringSize('origin', 512)
        if (type(site) is not type(str()) or len(site) > 2048) and site:
            raise IsNotStringSize('site', 2048)
        tmp = Invention(date=date, name=name, inventor=inventor, origin=origin, site=site)
        database.session.add(tmp)
        database.session.commit()
        output = {
            'data': {'success': True},
            'status_code': 200
        }
    except StacklabException as exception:
        output = {
            'data': {'success': False, 'message': exception.message},
            'status_code': exception.status_code
        }
    return output


@bp.route('/inventions', methods=['GET', 'POST'])
@bp.route('/inventions/<string:invention_id>', methods=['GET', 'PUT', 'DELETE'])
def invention(invention_id=None):
    if request.method == 'GET':
        if not invention_id:
            response_data = invention_get_all()
        else:
            response_data = invention_get(invention_id)
    if request.method == 'PUT':
        if invention_id == 'init':
            response_data = invention_init()
        else:
            response_data = {'data': {'success': False}, 'status_code': 400}
    if request.method == 'DELETE':
        response_data = invention_delete(invention_id)
    if request.method == 'POST':
        response_data = invention_add(request.json)
    response = jsonify(response_data['data'])
    response.status_code = response_data['status_code']
    return response
