from flask import Flask


def create_app(test_config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.update(test_config)

    # For testing sake
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    # Setup database
    from stacklab import db
    db.init_app(app)

    # Apply the blueprints to the app
    from stacklab import invention
    app.register_blueprint(invention.bp)

    return app
