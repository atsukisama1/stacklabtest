class StacklabException(Exception):
    message = str()
    status_code = int()
    pass


class InventionNotFound(StacklabException):
    def __init__(self):
        StacklabException.message = "Invention not found"
        StacklabException.status_code = 404


class DateNotInteger(StacklabException):
    def __init__(self):
        StacklabException.message = "Date is not an integer"
        StacklabException.status_code = 400


class JSONParameterNotFound(StacklabException):
    def __init__(self, parameter):
        StacklabException.message = "JSON parameter %s not found" % parameter
        StacklabException.status_code = 400


class IsNotStringSize(StacklabException):
    def __init__(self, parameter, length):
        StacklabException.message = "'%s' is not a string of size inferior to %d" % (parameter, length)
        StacklabException.status_code = 400


class JSONnotFound(StacklabException):
    def __init__(self):
        StacklabException.message = "JSON not found"
        StacklabException.status_code = 400