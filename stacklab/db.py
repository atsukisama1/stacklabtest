import json
from flask_sqlalchemy import SQLAlchemy

database = SQLAlchemy()


class Invention(database.Model):
    id = database.Column(database.Integer, primary_key=True)
    date = database.Column(database.Integer, unique=False, nullable=False)
    name = database.Column(database.String(256), unique=False, nullable=False)
    inventor = database.Column(database.String(64), unique=False, nullable=True)
    origin = database.Column(database.String(512), unique=False, nullable=True)
    site = database.Column(database.String(2048), unique=False, nullable=True)

    def __repr__(self):
        return '<Invention %d %r>' % (self.date, self.name)

    def to_dict(self):
        return {
            "id": self.id,
            "date": self.date,
            "name": self.name,
            "inventor": self.inventor,
            "origin": self.origin,
            "site": self.site
        }


def init_database():
    json_data = open('inventions.json').read()
    data = json.loads(json_data)
    for invention in data:
        date = None if 'date' not in invention else invention['date']
        name = None if 'name' not in invention else invention['name']
        inventor = None if 'inventor' not in invention else invention['inventor']
        origin = None if 'origin' not in invention else invention['origin']
        site = None if 'site' not in invention else invention['site']
        tmp = Invention(date=date, name=name, inventor=inventor, origin=origin, site=site)
        database.session.add(tmp)
        database.session.commit()


def init_app(app):
    with app.app_context():
        database.init_app(app)
        database.create_all()
        init_database()
