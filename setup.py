import io

from setuptools import find_packages, setup

setup(
    name='stacklab',
    version='1.0.0',
    maintainer='Paul-Sirawit KEREBEL',
    description='Basic test of flask for stacklab',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
        'flask_sqlalchemy'
    ],
    extras_require={
        'test': [
            'pytest',
            'coverage',
        ],
    },
)
