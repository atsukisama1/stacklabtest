import json
import string
import random


def string_generator(size):
    chars = string.ascii_uppercase + string.ascii_lowercase
    return ''.join(random.choice(chars) for _ in range(size))


def test_invention_init(client):
    response = client.put('/inventions/init')

    #File Data
    json_data = open('inventions.json').read()
    data = json.loads(json_data)
    data = sorted(data, key = lambda i: (i['date'], i['name']))

    #API data
    response_data = json.loads((client.get('/inventions')).data)
    for tmp_data in response_data:
        tmp_data.pop('id', None)
        if tmp_data['inventor'] is None: tmp_data.pop('inventor', None)
        if tmp_data['origin'] is None: tmp_data.pop('origin', None)
        if tmp_data['site'] is None: tmp_data.pop('site', None)
    response_data = sorted(response_data, key = lambda i: (i['date'], i['name']))

    assert response.status_code == 200
    assert data == response_data


def test_invention_delete_success(client):
    client.put('/inventions/init')
    response = client.delete('/inventions/1')
    response_check = client.get('/inventions/1')

    assert response.status_code == 200
    assert response_check.status_code == 404


def test_invention_delete_failed(client):
    client.put('/inventions/init')
    response = client.delete('/inventions/1')
    response_check = client.delete('/inventions/1')

    assert response.status_code == 200
    assert response_check.status_code == 404


def test_invention_get_success(client):
    client.put('/inventions/init')
    response = client.get('/inventions/1')
    response_check = client.get('/inventions/1')

    assert response.status_code == 200
    assert json.loads(response.data) == json.loads(response_check.data)


def test_invention_get_failed(client):
    client.put('/inventions/init')
    client.delete('/inventions/1')
    response = client.get('/inventions/1')

    assert response.status_code == 404


def test_invention_post_success(client):
    data = dict(date=1000, name='testing')
    response = client.post('/inventions', data=json.dumps(data), content_type='application/json')

    assert response.status_code == 200


def test_invention_post_failed_date_string(client):
    data = dict(date='test', name='testing')
    response = client.post('/inventions', data=json.dumps(data), content_type='application/json')

    assert response.status_code == 400


def test_invention_post_failed_long_name(client):
    data = dict(date=1000, name=string_generator(257))
    response = client.post('/inventions', data=json.dumps(data), content_type='application/json')

    assert response.status_code == 400


def test_invention_post_failed_long_inventor(client):
    data = dict(date=1000, name='testing',
                inventor=string_generator(65))
    response = client.post('/inventions', data=json.dumps(data), content_type='application/json')

    assert response.status_code == 400


def test_invention_post_failed_long_origin(client):
    data = dict(date=1000, name='testing',
                origin=string_generator(513))
    response = client.post('/inventions', data=json.dumps(data), content_type='application/json')

    assert response.status_code == 400


def test_invention_post_failed_long_site(client):
    data = dict(date=1000, name='testing',
                site=string_generator(2049))
    response = client.post('/inventions', data=json.dumps(data), content_type='application/json')

    assert response.status_code == 400


def test_invention_post_all_input(client):
    data = dict(date=1000,
                name=string_generator(256),
                inventor=string_generator(64),
                origin=string_generator(512),
                site=string_generator(2048))
    response = client.post('/inventions', data=json.dumps(data), content_type='application/json')

    assert response.status_code == 200
