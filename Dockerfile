FROM python:latest

WORKDIR /usr/src/app
COPY . /usr/src/app/
RUN pip install -e .

ENV FLASK_APP=stacklab

EXPOSE 5000
CMD [ "flask", "run", "--host=0.0.0.0", "--port=5000" ]
